\chapter{Probabilidad}%
\label{cha:Probabilidad}

\section{Repaso de teoría de conjuntos}%
\label{sec:teoria-conj}

Aquí revisaremos los requisitos básicos de la teoría de conjuntos.

\section{Función de probabilidad}%
\label{sec:funcion_probabilidad}

Nos interesa considerar la probabilidad de que un evento particular ocurra, dado que hemos realizado algún experimento.
Llamaremos espacio de muestras a el conjunto de todos los resultados posibles de nuestro experimento, y lo denotaremos por $\mathcal{C}$.
Nos interesa entonces una función que asigne probabilidades a los los subconjuntos de $\mathcal{C}$, es decir, a los resultados posibles de nuestro experimento.

Si ocurriese que $\mathcal{C}$ es \emph{finito}, entonces bastaría conseguir una función que asigne una probabilidad a cada uno de los subconjuntos de $\mathcal{C}$.
Sin embargo, si nuestro espacio de muestra es \emph{infinito} entonces buscar una función con la propiedad anterior es una tarea que se encuentra rápidamente con varias sutilezas matématicas que no viene al caso explicar.

La nocion de una función de probabilidad, que se explicará en breve, puede explicarse de manera más intuitiva partiendo del concepto de \emph{frequencia}.
Si realizamos un experimento y queremos saber la frequencia con que ocurrió evento $A$ (es decir, $A\subset\mathcal{C}$) entonces basta con dividir la cantidad de veces que ocurrió el evento $A$ por la cantidad de veces que se repitió el experimento.
Notemos que la frequencia del evento $A$ es mayor o igual que cero y menor que uno---recordemos que es un cociente, para la frecuencia ser mayor que $1$ el evento $A$ tendría que haber ocurrido \emph{mas veces} de las que se repitió el experimento. 
Además al frequencia del espacio de muestra es igual a $1$ pues estamos seguros de que seguros de que este ocurre, trivialmente, cada vez que se repite el experimento. 
Por último, si $A$ y $B$ son dos eventos independientes, esto es, que la realizacion de uno no afecta al otro, entonces la frequencia con que ocurren ambos es la suma de las frequencias de cada uno.

\begin{defi}
	Sea $\mathcal{C}$ un espacio de muestras. Una función $P\colon\mathcal{P}(\mathcal{C})\to\mathbb{R}$ es llamada \Keyword{de probabilidad} si cumple con la siguientes propiedades:
	\begin{enumerate}
		\item $P(A)\geq0$ para todo $A\in\mathcal{C}$,
		\item $P(\mathcal{C})=1$  y
		\item Si la familia $\{ A_k \}(k\in\mathscr{K})$ de subconjuntos de $\mathcal{C}$ es disjunta, entonces
			\[ P(\bigcup_{k\in\mathscr{K}} A_k)=\sum_{k\in\mathscr{K}} P(A_k) \]
	\end{enumerate}
\end{defi}

\Nota{En la definición anterior la familia de índices $\mathscr{K}$ puede ser finita o infinita, pero debe ser numerable. 
Cuando nos referimos a una familia disjunta ---aquí, y a partir de ahora--- nos refererimos simplemente a una familia disjunta \emph{dos a dos}, es decir, $A_k\cap A_l=\emptyset$ para todo $k\neq l$ donde $k,l$ pertenecen a $\mathscr{K}$.}

Los siguientes teoremas nos dan varias propiedades de la funcion de probabilidad.

\begin{teo}
	Para cada evento $A$ se tiene que $P(A)=1-P(A^c)$
\end{teo}
\begin{proof}
	Como la unión $A\cup A^c$ es disjunta, se sigue que
	\[
		P(A\cup A^c)=P(A)+P(A^c).
	\]
\end{proof}
\begin{teo}
	La probabilidad del conjunto vacío es cero.
\end{teo}
\begin{teo}
	Si $A$ y $B$ son eventos tales que $A\subset B$ entonces
	\[
		P(A)\leq P(B).
	\]
\end{teo}

\Nota{Los tres teoremas anteriores tienen demostraciones muy sencillas, todas consecuencias directas de aplicar las propiedades de la función de probabilidad.}

El siguiente teorema, que usa la llamada \emph{regla de inclusión-exclusión}, nos dice como es al probabilidad de una unión de eventos incluso cuando estos no son disjuntos.

\begin{teo}
	Si $A$ y $B$ son dos eventos, posiblemente no disjuntos, entonces
	\[
		P(A\cup B) = P(A)+P(B)-P(A\cap B).
	\]
\end{teo}

En el caso de que tengamos un espacio de muestras finito podemos definir una función de probabilidad $P$ de la siguiente manera.
Digamos que $\mathcal{C}=\left\{ x_1,\dots,x_m \right\}$, y tomemos $m$ números racionales $p_i$ en el intervalo $[0,1]$ tales que
\[
	p_1+p_2+\dots+p_m=1,
\]
de esta forma podemos establecer una correlación `uno a uno' entre los $x_i$ y los $p_i$.

Entonces la función $P$ dada, para todo subconjunto $A$ de $\mathcal{C}$, por
\[
	P(A)= \sum_{x_i\in A} p_i
\]
es una función de probabilidad. Es claro que $P(A)>0$, pues se están sumando fracciones positivas, y además $P(C)=1$ puesto que esta es precisamente la condición bajo la que se eligieron los $p_i$. 
Ahora, si consideramos la union de una familia de subconjuntos disjuntos de $\mathcal{C}$,tendremos que se cumple también la tercera propiedad de las funciones de probabilidad.
Esto es fácil de ver una vez que notemos que, al ser disjuntos, los subconjuntos de $\mathcal{C}$ que estamos considerando contienen \emph{diferentes} puntos $x_i$ y por lo tanto también diferentes $p_i$. 
Entonces los $p_i$ asociados a cada subconjunto no se repiten al unir a estos últimos, lo que hace que sumar sobre los elementos de la unión sea lo mismo que sumar cada una por separado,
\[
	P(\bigcup_{k\in\mathscr{K}} A_k) = \sum_{k\in\mathscr{K}} P(A_k).
\]

Si en la construcción anterior hacemos que los $p_i$ sean todos iguales a $1/m$ entonces obtenemos el caso \Keyword{equiprobable}, como lo es por ejemplo el lanzar un dado una vez: cada cara tiene probabilidad $1/6$, donde la probabilidad de un evento $A$ viene dada por
\[
	P(A)= \frac{\#\{A\}}{m}.
\]
